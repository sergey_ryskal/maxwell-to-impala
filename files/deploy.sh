#!/usr/bin/env bash
ACCESS_PORTAL='ubuntu@34.197.227.102'
IMPALA_MASTER='192.0.0.132'
PRIVATE_KEY='impala_cluster.pem'
DEFAULT_PORT=38952

function clean_up () {
  pkill -f ${DEFAULT_PORT}:${IMPALA_MASTER} > /dev/null 2>&1
  rm -R ../target > /dev/null 2>&1
}

clean_up

./build.sh \
&& ssh -o StrictHostKeyChecking=no -i ${PRIVATE_KEY} -f -N -L ${DEFAULT_PORT}:${IMPALA_MASTER}:22 ${ACCESS_PORTAL} \
&& scp -o StrictHostKeyChecking=no -P ${DEFAULT_PORT} -i ${PRIVATE_KEY} ../target/maxwell-to-impala-1.0.jar centos@127.0.0.1:/home/centos/impala_importer.jar \
&& scp -o StrictHostKeyChecking=no -P ${DEFAULT_PORT} -i ${PRIVATE_KEY} ./start_app.sh centos@127.0.0.1:/home/centos \
&& clean_up

