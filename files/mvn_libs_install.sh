#!/usr/bin/env bash
mvn install:install-file \
  -Dfile=lib/hive_metastore.jar \
  -DgroupId=org.apache.hadoop.hive \
  -DartifactId=hive_metastore \
  -Dversion=1.0 \
  -Dpackaging=jar \
  -DgeneratePom=true

mvn install:install-file \
  -Dfile=lib/hive_service.jar \
  -DgroupId=org.apache.hadoop.hive \
  -DartifactId=hive_service \
  -Dversion=1.0 \
  -Dpackaging=jar \
  -DgeneratePom=true

mvn install:install-file \
  -Dfile=lib/ImpalaJDBC41.jar \
  -DgroupId=com.cloudera \
  -DartifactId=impala_driver \
  -Dversion=2.5.36.1056 \
  -Dpackaging=jar \
  -DgeneratePom=true

mvn install:install-file \
  -Dfile=lib/ql.jar \
  -DgroupId=org.apache.hadoop.hive \
  -DartifactId=ql \
  -Dversion=1.0 \
  -Dpackaging=jar \
  -DgeneratePom=true

mvn install:install-file \
  -Dfile=lib/TCLIServiceClient.jar \
  -DgroupId=org.apache.hadoop.hive \
  -DartifactId=service_client \
  -Dversion=1.0 \
  -Dpackaging=jar \
  -DgeneratePom=true

