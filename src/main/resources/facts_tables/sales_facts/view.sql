CREATE VIEW SalesFactsTableView AS (
  SELECT
    t.id                                                                                      AS txId,
    s.quantity                                                                                AS quantity,
    s.dtaClientId                                                                             AS dtaClientId,
    s.dtaEmployeeId                                                                           AS dtaEmployeeId,
    x.Description                                                                             AS ServiceName,
    t._date                                                                                   AS txDate,
    s.saleType                                                                                AS SaleType,
    s.Total                                                                                   AS PostDiscount,
    x.cfgServiceCategoriesId                                                                  AS cfgServiceCategoriesId,
    s.cfgPromotionId                                                                          AS cfgPromotionId,
    c.SpecialType                                                                             AS SpecialType,
    if(ba.isDeleted, 0, ba.TipAmount)                                                         AS tipAmount,
    t.SalesTax                                                                                AS txSalesTax,
    t.PaymentGiftCard                                                                         AS TxPaymentGiftCard,
    t.PaymentOther                                                                            AS TxPaymentOther,
    c.FirstName                                                                               AS FirstName,
    c.LastName                                                                                AS LastName,
    s.commissionPrice                                                                         AS commissionPrice,
    s.priceEntered                                                                            AS priceEntered,
    s.cfgServiceId                                                                            AS cfgServiceId,
    s.lkpServiceEmployeeClientQueueId                                                         AS lkpServiceEmployeeClientQueueId,
    promo.Description                                                                         AS promoName,
    cat.name                                                                                  AS catName,
    t.store_id                                                                                AS storeId,
    t._timestamp                                                                              AS txtimestamp,
    s.TaxRate                                                                                 AS sTaxRate,
    tcq.id                                                                                    AS tcqId,
    tcq.FinishServiceTime                                                                     AS finishServiceTime,
    tcq.ServicedTime                                                                          AS servicedTime,
    x.Duration                                                                                AS cfgServiceDuration,
    x.GapDuration                                                                             AS cfgServiceGapDuration,
    x.AfterDuration                                                                           AS cfgServiceAfterDuration,
    concat_ws('_', cast (t.dp_id as string), cast(t.store_id as string))                      AS dpIdAndStoreId,
    de.dtaEmployeeLevelId                                                                     AS dtaEmployeeLevelId,
    t.dp_id                                                                                   AS dpId,
    datediff(t._date, '1990-01-01')                                                           AS txDateDays
  FROM TxnTransactions t
    INNER JOIN TxnSales s ON s.txnTransactionId = t.id AND s.dp_id = t.dp_id
    INNER JOIN DtaClients c ON s.dtaClientId = c.id AND s.dp_id = c.dp_id
    LEFT JOIN CfgServices x ON s.cfgServiceId = x.id AND s.dp_id = x.dp_id
    LEFT JOIN TxnBenefitAudits ba ON s.txnBenefitAuditId = ba.Id AND s.dp_id = ba.dp_id
    LEFT JOIN CfgPromotions promo ON s.cfgPromotionId = promo.Id AND s.dp_id = promo.dp_id
    LEFT JOIN CfgServiceCategory cat ON x.cfgServiceCategoriesId = cat.id AND s.dp_id = cat.dp_id
    LEFT JOIN LkpServiceEmployeesClientQueues se ON se.id = s.lkpServiceEmployeeClientQueueId AND s.dp_id = se.dp_id
    LEFT JOIN TxnClientQueue tcq ON tcq.id = se.txnClientQueueId AND se.dp_id = tcq.dp_id
    LEFT JOIN DtaEmployees de ON s.dtaEmployeeId = de.id AND s.dp_id = de.dp_id
  WHERE !t.voided AND !t.isDeleted AND !s.isDeleted
);