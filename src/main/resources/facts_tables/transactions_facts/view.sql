CREATE VIEW TransactionsFactsTableView AS (
  SELECT
    tx.Id                                           AS txId,
    tx.store_id                                     AS storeId,
    tx.dp_id                                        AS dpId,
    concat_ws('_', cast (tx.dp_id as string),
              cast(tx.store_id as string))          AS dpIdAndStoreId,
    tx.`_date`                                      AS txDate,
    tx.SalesTax,
    tx.PaymentOther,
    c.FirstName                                     AS PayAccountName,
    c.LastName                                      AS LastName,
    c.SpecialType                                   AS PayClientSpecialType,
    c1.FirstName                                     AS TargetPayAccountName,
    c1.LastName                                      AS TargetLastName,
    c1.SpecialType                                   AS TargetPayClientSpecialType,
    ShiftTipsMode,
    TipSelectionMode,
    TotalTender,
    PaymentSeries,
    PaymentCredit,
    PaymentGiftCard,
    PaymentOtherTypeId,
    datediff(tx._date, '1990-01-01')                 AS txDateDays
  FROM TxnTransactions tx
    LEFT JOIN DtaClients c ON tx.dtaClientId = c.id AND tx.dp_id = c.dp_id
    LEFT JOIN DtaClients c1 ON tx.targetAccountId = c1.id AND tx.dp_id = c1.dp_id
    LEFT JOIN TxnShifts ts ON tx.txnShiftId = ts.id AND tx.dp_id = ts.dp_id
  WHERE !tx.voided AND !tx.isDeleted);