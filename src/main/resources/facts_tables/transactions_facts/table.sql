CREATE TABLE TransactionsFactsTable (
  txid                       BIGINT,
  storeid                    BIGINT,
  dpid                       BIGINT,
  dpIdAndStoreId             string,
  txdate                     TIMESTAMP,
  salestax                   DECIMAL(19, 4),
  paymentother               DECIMAL(19, 4),
  payaccountname             VARCHAR(64),
  lastname                   VARCHAR(64),
  payclientspecialtype       SMALLINT,
  TargetPayAccountName       VARCHAR(64),
  TargetLastName             VARCHAR(64),
  TargetPayClientSpecialType SMALLINT,
  shifttipsmode              SMALLINT,
  tipselectionmode           SMALLINT,
  totaltender                DECIMAL(19, 4),
  paymentseries              DECIMAL(19, 4),
  paymentcredit              DECIMAL(19, 4),
  paymentgiftcard            DECIMAL(19, 4),
  paymentothertypeid         SMALLINT
)
  partitioned BY (txDateDays INT
)
  STORED AS PARQUET;