CREATE VIEW StatSalesPreCalc_NewEmployeeRegistryPartedView AS (
SELECT *,
    concat_ws('_', (cast(dp_id AS STRING)),
        (cast(store_id AS STRING)))                AS dpIdAndStoreId,
    datediff(mindate, '1990-01-01') txdatedays
FROM StatSalesPreCalc_NewEmployeeRegistry);