CREATE TABLE StatSalesPreCalc_NewEmployeeRegistryParted
(
  store_id             BIGINT,
  dtaclientid          BIGINT,
  mindate              TIMESTAMP,
  mindateglobal        TIMESTAMP,
  employeeid           BIGINT,
  employeename         VARCHAR(129),
  firsttxnid           BIGINT,
  nexttxndate          TIMESTAMP,
  newbypromoid         VARCHAR(255),
  firstservicetxnid    BIGINT,
  minservicedate       TIMESTAMP,
  minservicedateglobal TIMESTAMP,
  mintxntimestamp      TIMESTAMP,
  fromMerge            BOOLEAN,
  isinvalid            BOOLEAN,
  dp_id                BIGINT
)
  partitioned BY (dateDays INT
)
  STORED AS PARQUET;