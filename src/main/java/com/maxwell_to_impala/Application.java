package com.maxwell_to_impala;

import com.maxwell_to_impala.runners.AbstractRunner;
import com.maxwell_to_impala.runners.RunnerHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(lazyInit = true, value = "com.maxwell_to_impala.*")
@Slf4j
public class Application {
    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication();
        sa.setWebEnvironment(false);
        ApplicationContext ctx = sa.run(Application.class, args);
        if(args.length == 0) {
            throw new IllegalStateException("Please specify application action. ");
        }
        for(String command: args) {
            Class<? extends AbstractRunner> runnerClass = RunnerHolder.getRunner(command.toUpperCase());
            if(runnerClass == null) {
                log.warn("There is no runner '{}'", command);
            }
            AbstractRunner runner = ctx.getBean(runnerClass);
            runner.perform();
        }
    }
}
