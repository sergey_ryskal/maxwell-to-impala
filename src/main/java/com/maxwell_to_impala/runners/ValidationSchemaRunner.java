package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.holder.MySqlConnectionsHolder;
import com.maxwell_to_impala.model.ColumnDefinition;
import com.maxwell_to_impala.model.MySqlDbMetadata;
import com.maxwell_to_impala.service.MySqlExportService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Lazy
@Slf4j
@Service
public class ValidationSchemaRunner extends AbstractRunner
{
    @Autowired
    private MySqlExportService mySqlService;

    @Autowired
    private MySqlConnectionsHolder connectionsHolder;

    @Override
    public void perform() {
        List<MySqlDbMetadata> dbMetadata = connectionsHolder.getDbMetadata();
        Map<String, Map<String, List<ColumnDefinition>>> tablesByDb = new HashMap<>();
        AtomicInteger counter = new AtomicInteger();
        dbMetadata.forEach(mySqlDbMetadata -> {
            tablesByDb.put(mySqlDbMetadata.getDbName(), mySqlService.getTablesDefinition(mySqlDbMetadata));
            log.info("Retrieve columns definition. Finished {}/{} dbs.", counter.incrementAndGet(), dbMetadata.size());
        });
        String randomDbName = new ArrayList<>(tablesByDb.keySet()).get(tablesByDb.keySet().size() - 1);
        Map<String, List<ColumnDefinition>> randomDatabase = tablesByDb.get(randomDbName);

        tablesByDb.forEach((dbName, tables) -> {
            Set<String> randomDatabaseTables = randomDatabase.keySet();
            Set<String> currentDatabaseTables = tables.keySet();
            Collection tablesDifferences = CollectionUtils.disjunction(randomDatabaseTables, currentDatabaseTables);
            if(tablesDifferences.size() > 0) {
                log.info("Dbs: {}:{}. Tables: " + tablesDifferences, dbName, randomDbName);
            }
            tables.forEach((tableName, columns) -> {
                List<ColumnDefinition> randomDatabaseColumns = randomDatabase.get(tableName);
                if(randomDatabaseColumns == null) {
                    log.info("Table missed: {}", tableName);
                    return;
                }
                Collection differences = CollectionUtils.disjunction(columns, randomDatabaseColumns);
                if(differences.size() > 0) {
                    log.info("Dbs: {}:{}. " + "Table: " + tableName + " differences columns count: " + differences.size(), dbName, randomDbName);
                    if(differences.size() < 3) {
                        log.info("Columns: {}", differences);
                    }
                }
            });
            log.info(SEPARATOR);
        });

    }
}
