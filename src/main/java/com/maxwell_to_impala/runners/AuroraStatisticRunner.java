package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.holder.MySqlConnectionsHolder;
import com.maxwell_to_impala.model.MySqlDbMetadata;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Lazy
@Slf4j
@Service
public class AuroraStatisticRunner extends AbstractRunner {
    @Autowired
    private MySqlConnectionsHolder connectionsHolder;

    @Override
    public void perform() {
        List<MySqlDbMetadata> dbMetadata = connectionsHolder.getDbMetadata();
        Map<MySqlDbMetadata, Integer> transactionsByDb = new HashMap<>();
        Map<MySqlDbMetadata, Integer> storeByDb = new HashMap<>();
        Integer allStoresCount = 0;
        Integer allTransactionsCount = 0;
        Integer performedDbsCounter = 0;

        for (MySqlDbMetadata mySqlDbMetadata : dbMetadata) {
            try (Connection connection = mySqlDbMetadata.getConnection(); Statement txPrepSt = connection.createStatement(); Statement storePrepSt = connection.createStatement()) {
                ResultSet txnResult = txPrepSt.executeQuery("select count(*) from " + mySqlDbMetadata.getDbName() + ".TxnTransactions");
                ResultSet storeResult = storePrepSt.executeQuery("select count(*) from " + mySqlDbMetadata.getDbName() +  ".AstStores");

                txnResult.next();
                storeResult.next();

                int storesCount = storeResult.getInt(1);
                int transactionsCount = txnResult.getInt(1);
                allStoresCount += storesCount;
                allTransactionsCount += transactionsCount;

                transactionsByDb.put(mySqlDbMetadata, transactionsCount);
                storeByDb.put(mySqlDbMetadata, storesCount);

                txnResult.close();
                storeResult.close();
                log.info("Processed dbs: {}/{}", ++performedDbsCounter, dbMetadata.size());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        transactionsByDb.forEach((mySqlDbMetadata, transactionsCount) -> {
            log.info("Db: {}, TxCount: {}, StoreCount: " + storeByDb.getOrDefault(mySqlDbMetadata, 0), mySqlDbMetadata.getDbName(), transactionsCount);
        });
        log.info("Dbs Count: {}, AllTxCount: {}, AllStoresCount: " + allStoresCount, dbMetadata.size(), allTransactionsCount);

    }
}
