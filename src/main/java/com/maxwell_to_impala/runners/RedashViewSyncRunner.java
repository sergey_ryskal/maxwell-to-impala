package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.model.RedashQuery;
import com.maxwell_to_impala.holder.ImpalaSchemaHolder;
import com.maxwell_to_impala.service.RedashAPIService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RedashViewSyncRunner extends AbstractRunner {

    @Lazy
    @Autowired
    private ImpalaSchemaHolder impalaSchemaHolder;

    @Lazy
    @Autowired
    private RedashAPIService redashService;

    private String retrieveSelectQueryFromView(String createViewCommand) {
        String breakSymbols = "AS";
        return createViewCommand.substring(createViewCommand.indexOf(breakSymbols) + breakSymbols.length());
    }

    private String wrapWithBrackets(String str) {
        return "(" + str + ")";
    }

    @Override
    public void perform() {
        impalaSchemaHolder.getViews().forEach(viewEntity -> {
            String view = retrieveSelectQueryFromView(viewEntity.getCreateStatement());
            RedashQuery redashQuery = new RedashQuery();
            redashQuery.setName(viewEntity.getName());
            redashQuery.setQuery(wrapWithBrackets(view));
            redashQuery.setDatasourceId(redashService.getDatasourceId());
            redashService.upsertQuery(redashQuery);
        });
        log.info("Redash sync finished. Processed {} queries. ", impalaSchemaHolder.getViews().size());
    }
}
