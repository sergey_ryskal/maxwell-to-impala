package com.maxwell_to_impala.runners;

import java.util.HashMap;
import java.util.Map;

public class RunnerHolder {
    private static final Map<String, Class<? extends AbstractRunner>> runners = new HashMap<>();

    static {
        runners.put("EXPORT", MySqlExportRunner.class);
        runners.put("VALIDATE_EXPORT", ImportValidationRunner.class);
        runners.put("AURORA_STAT", AuroraStatisticRunner.class);
        runners.put("TABLES_PARTITIONING", ImpalaTablesPartitioningRunner.class);
        runners.put("VALIDATION_SCHEMA", ValidationSchemaRunner.class);
        runners.put("FACTS_TABLE_CREATION", FactsTableCreationRunner.class);
        runners.put("COMPUTE_STATS", ComputeStatsRunner.class);
        runners.put("INVALIDATE_METADATA", InvalidateMetadataRunner.class);
        runners.put("REDASH_SYNC", RedashViewSyncRunner.class);
    }

    public static Class<? extends AbstractRunner> getRunner(String command) {
        return runners.get(command);
    }
}
