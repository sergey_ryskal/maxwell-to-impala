package com.maxwell_to_impala.runners;


import com.maxwell_to_impala.holder.ImpalaSchemaHolder;
import com.maxwell_to_impala.service.ImpalaImportService;
import com.maxwell_to_impala.utils.RetriesUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Service
public class ComputeStatsRunner extends AbstractRunner {

    @Autowired
    private ImpalaImportService impalaImportService;

    @Autowired
    private ImpalaSchemaHolder impalaSchemaHolder;

    @Override
    public void perform() {
        AtomicLong counter = new AtomicLong();
        List<String> impalaList = impalaSchemaHolder.getTables();
        impalaList.parallelStream().forEach(tableName -> RetriesUtils.executeWithRetries(() -> {
            long tablesProcessCounter = counter.incrementAndGet();
            String excludeTable = "SalesFactsTable";
            if (!tableName.contains(excludeTable) && !tableName.contains(excludeTable.toLowerCase())) {
                log.info("Start process {}: {}/{}", tableName, tablesProcessCounter, impalaList.size());
                impalaImportService.execute("INVALIDATE METADATA " + tableName);
                impalaImportService.execute("COMPUTE STATS " + tableName);
            }
        }));
    }
}
