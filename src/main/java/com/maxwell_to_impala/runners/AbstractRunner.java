package com.maxwell_to_impala.runners;

public abstract class AbstractRunner {
    protected static final String SEPARATOR = "------------------------------------------------";

    public abstract void perform();
}
