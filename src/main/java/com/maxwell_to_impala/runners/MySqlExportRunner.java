package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.config.HadoopConfiguration;
import com.maxwell_to_impala.model.ColumnDefinition;
import com.maxwell_to_impala.model.MySqlDbMetadata;
import com.maxwell_to_impala.holder.MySqlConnectionsHolder;
import com.maxwell_to_impala.service.HadoopFsService;
import com.maxwell_to_impala.service.ImpalaImportService;
import com.maxwell_to_impala.service.MySqlExportService;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Lazy
@Slf4j
@Service
public class MySqlExportRunner extends AbstractRunner {
    public static final String EXTERNAL_TABLE_KEY = "EXTERNAL_" +  Math.abs(UUID.randomUUID().hashCode());

    @Autowired
    private MySqlExportService mySqlService;

    @Autowired
    private ImpalaImportService impalaImportService;

    @Autowired
    private HadoopFsService hadoopFsService;

    @Autowired
    private HadoopConfiguration hadoopConfiguration;

    @Autowired
    private MySqlConnectionsHolder connectionsHolder;

    @PostConstruct
    public void init() {
        hadoopFsService.rmr(hadoopConfiguration.getBaseDir());
    }


    @Override
    public void perform() {
            List<MySqlDbMetadata> dbMetadata = connectionsHolder.getDbMetadata();

            Map<String, List<ColumnDefinition>> tablesDefinitions = mySqlService.getTablesDefinition(dbMetadata.get(0));
            ExecutorService executor = Executors.newFixedThreadPool(32);

            AtomicLong counter = new AtomicLong();
            dbMetadata.forEach(db -> hadoopFsService.mkdir(hadoopConfiguration.getBaseDir() + File.separator + db.getDbName()));

            tablesDefinitions.forEach((tableName, columns) -> executor.execute(() -> {
                log.info("Start importing: {}", tableName);
                AtomicInteger tableCounter = new AtomicInteger();
                List<ColumnDefinition> tableDefinitionWithNewColumn = new ArrayList<>(columns);
                ColumnDefinition additionalColumn = new ColumnDefinition(tableName, "dp_id", "bigint", "bigint");
                tableDefinitionWithNewColumn.add(additionalColumn);
                impalaImportService.createParquetTable(tableDefinitionWithNewColumn, tableName);

                dbMetadata.forEach(mySqlDbMetadata -> {
                    String resultFilePath = null;
                    try {
                        Pair<String, String> mySqlTableAsCsvFile = mySqlService.tableToTsv(tableName, mySqlDbMetadata, columns);
                        if(mySqlTableAsCsvFile == null) {
                            return;
                        }
                        resultFilePath = mySqlTableAsCsvFile.getKey();
                        String dbName = mySqlTableAsCsvFile.getValue();

                        String hadoopTableFileDirectory = hadoopConfiguration.getBaseDir() + dbName + File.separator + tableName;
                        String hadoopFilePath = hadoopTableFileDirectory + File.separator + tableName;
                        hadoopFsService.uploadFile(resultFilePath, hadoopFilePath, false);

                        String externalTableName = tableName + EXTERNAL_TABLE_KEY + dbName;

                        impalaImportService.createExternalTable(externalTableName, tableDefinitionWithNewColumn, hadoopTableFileDirectory);
                        impalaImportService.insertIntoSelect(externalTableName, tableName);

                        impalaImportService.dropTableIfExists(externalTableName);
                    } catch (Exception e) {
                        log.warn("Can not process {} table export", tableName, e);
                    } finally {
                        tableCounter.incrementAndGet();
                        if(resultFilePath != null) {
                            new File(resultFilePath).delete();
                        }
                        log.info("Finished import {}. {}/" + dbMetadata.size(), tableName, tableCounter.get());
                    }
                });
                log.info("Impala Rows for table " + tableName + ":" + impalaImportService.getRowsCount(tableName)
                        + ". Finished importing: {}/{}.", counter.incrementAndGet(), tablesDefinitions.size());
            }));

            executor.shutdown();
            try {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            log.info("Export finished");
    }
}
