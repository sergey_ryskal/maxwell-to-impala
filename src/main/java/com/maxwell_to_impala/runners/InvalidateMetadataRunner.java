package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.holder.ImpalaSchemaHolder;
import com.maxwell_to_impala.service.ImpalaImportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Lazy
@Slf4j
@Service
public class InvalidateMetadataRunner extends AbstractRunner {

    @Autowired
    private ImpalaImportService impalaImportService;

    @Autowired
    private ImpalaSchemaHolder impalaSchemaHolder;

    @Override
    public void perform() {
        AtomicLong counter = new AtomicLong();
        List<String> impalaList = impalaSchemaHolder.getTables();
        impalaList.forEach(tableName -> {
            try {
                log.info("Start process {}: {}/{}", tableName, counter.incrementAndGet(), impalaList.size());
                impalaImportService.execute("INVALIDATE METADATA " + tableName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
