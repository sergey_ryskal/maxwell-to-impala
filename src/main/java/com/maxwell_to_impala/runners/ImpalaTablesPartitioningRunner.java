package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.config.ApplicationConfig;
import com.maxwell_to_impala.holder.ImpalaSchemaHolder;
import com.maxwell_to_impala.model.ColumnDefinition;
import com.maxwell_to_impala.service.ImpalaImportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


@Lazy
@Slf4j
@Service
public class ImpalaTablesPartitioningRunner extends AbstractRunner {

    public final String PARTITIONED_TABLE_KEY = "_partitioned";

    @Lazy
    @Autowired
    private ApplicationConfig applicationConfig;

    @Lazy
    @Autowired
    private ImpalaImportService impalaImportService;

    @Lazy
    @Autowired
    private ImpalaSchemaHolder impalaSchemaHolder;

    private Map<String, List<ColumnDefinition>> tables = new HashMap<>();

    @PostConstruct
    public void init() {
        impalaSchemaHolder.getTables().parallelStream().forEach(tableName ->
                tables.put(tableName, impalaImportService.getColumns(tableName)));
    }

    @Override
    public void perform() {
        AtomicLong counter = new AtomicLong(1);

        tables.forEach((sourceTable, columnDefinitions) -> {
            try {
                String destinationPartitionedTableName = sourceTable + PARTITIONED_TABLE_KEY;
                log.info("Start partitioning table: {}", sourceTable);
                boolean wasPartitioned = impalaImportService.createPartitioningTable(columnDefinitions,
                        applicationConfig.getPartitionColumns(),
                        sourceTable,
                        destinationPartitionedTableName);
                if(wasPartitioned) {
                    int sourceTableRowsCount = impalaImportService.getRowsCount(sourceTable);
                    int destinationTableRowsCount = impalaImportService.getRowsCount(destinationPartitionedTableName);

                    if(sourceTableRowsCount == destinationTableRowsCount) {
                        impalaImportService.dropTableIfExists(sourceTable);
                        String newTable = impalaImportService.renameTable(destinationPartitionedTableName, sourceTable);
                        impalaImportService.computeStats(newTable);
                        log.info("Table {} has been successfully partitioned. {}/{}", sourceTable, counter.get(), tables.size());
                    } else {
                        log.warn("Failed to partition Table: {}, SourceTable/PartitionedTable {}/{}",
                                sourceTable,sourceTableRowsCount,destinationTableRowsCount);
                        impalaImportService.dropTableIfExists(destinationPartitionedTableName);
                    }
                } else {
                    log.info("Failed to partitioning table: {}. {}/{}", sourceTable, counter.get(), tables.size());
                }
            } catch (Exception e) {
                log.warn("Failed to partition: {}", sourceTable, e);
            } finally {
                counter.incrementAndGet();
            }
        });
    }
}
