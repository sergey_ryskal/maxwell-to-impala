package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.config.ApplicationConfig;
import com.maxwell_to_impala.holder.FactsTablesDefinitionsHolder;
import com.maxwell_to_impala.model.FactsTable;
import com.maxwell_to_impala.service.ImpalaImportService;
import com.maxwell_to_impala.utils.DatabaseUtils;
import com.maxwell_to_impala.utils.IOUtils;
import javafx.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Lazy
@Slf4j
@Service
public class FactsTableCreationRunner extends AbstractRunner {

    public static final String AMOUNT_KEY = "Amount";

    @Autowired
    private ApplicationConfig applicationConfig;

    @Lazy
    @Autowired
    private FactsTablesDefinitionsHolder factsTablesHolder;

    @Lazy
    @Autowired
    private ImpalaImportService impalaService;

    @Lazy
    @Autowired
    private IOUtils ioUtils;

    @Override
    public void perform() {
        List<FactsTable> factsTables = factsTablesHolder.getFactsTables();
        factsTables.forEach(factsTable -> {
            log.info("Start creating {};", factsTable.getTableName());
            impalaService.execute(ioUtils.getFileContent(factsTable.getViewFilePath()));
            impalaService.execute(ioUtils.getFileContent(factsTable.getTableFilePath()));
            log.info("Defining range for '{}'", factsTable.getRangeConditionColumn());
            List<RowCardinality> conditions = impalaService.executeSelect(conditionQuery(factsTable.getRangeConditionColumn(),
                    factsTable.getViewName()), result -> {
                Object columnValue = result.get(factsTable.getRangeConditionColumn());
                return new RowCardinality(factsTable.conditionColumnValueToString(columnValue), (Long) result.get(AMOUNT_KEY));
            });
            List<Pair<String, String>> conditionsByInterval = defineConditionInterval(conditions);

            conditionsByInterval.forEach(interval -> {
                try {
                    log.info("Insert to {}, for range ({} - {})", factsTable.getTableName(), interval.getKey(), interval.getValue());
                    String insertToStatement = insertIntoTableFromView(factsTable, interval);
                    impalaService.execute(insertToStatement);
                } catch (Exception e) {
                    log.error("Failed to insert into {}, for range ({} - {})",
                            factsTable.getTableName(), interval.getKey(), interval.getValue());
                    e.printStackTrace();
                }
            });
            validateFactsTableCreation(factsTable);
        });
    }

    private void validateFactsTableCreation(FactsTable factsTable) {
        String factsTableName = factsTable.getTableName();
        String validationTableName = factsTable.getValidationByCountTableName();

        int factsTableRowsCount = impalaService.getRowsCount(factsTableName);
        int validationTableRowsCount = impalaService.getRowsCount(validationTableName);
        if(factsTableRowsCount == validationTableRowsCount) {
            log.info("Facts Table: {} successfully created. ", factsTableName);
        } else {
            log.info("Rows count: {}/{} {}/{}", factsTableName, validationTableName, factsTableRowsCount, validationTableRowsCount);
        }
    }

    private List<Pair<String, String>> defineConditionInterval(List<RowCardinality> conditions) {
        List<Pair<String, String>> result = new ArrayList<>();
        String startKey = null;
        AtomicLong rowsCount = new AtomicLong();

        for (RowCardinality condition : conditions) {
            if(startKey == null) {
                startKey = condition.getRowValue();
                rowsCount.addAndGet(condition.getCount());
                continue;
            }
            long currentRowsCount = rowsCount.addAndGet(condition.getCount());
            if(currentRowsCount >= applicationConfig.getPartitionThreshold()) {
                result.add(new Pair<>(condition.getRowValue(), startKey));
                startKey = null;
                rowsCount.set(0L);
            }
        }

        if(startKey != null) {
            result.add(new Pair<>(startKey, conditions.get(conditions.size() - 1).getRowValue()));
        }
        return result;
    }

    private static String insertIntoTableFromView(FactsTable factsTable, Pair<String, String> conditionInterval) {
        String statement = "INSERT INTO %s " +
                "PARTITION (%s) " +
                "SELECT * " +
                "FROM %s " +
                "WHERE %s BETWEEN %s AND %s ";
        return String.format(statement, factsTable.getTableName(), DatabaseUtils.listToString(factsTable.getPartitionFields()),
                factsTable.getViewName(), factsTable.getRangeConditionColumn(), conditionInterval.getKey(), conditionInterval.getValue());
    }

    private String conditionQuery(String key, String viewName) {
        String template = "select %1$2s, count(*) AS " + AMOUNT_KEY + " from %2$2s WHERE %1$2s GROUP BY %1$2s ORDER BY %1$2s DESC;";
        return String.format(template, key, viewName);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private class RowCardinality {
        private String rowValue;
        private Long count;
    }
}
