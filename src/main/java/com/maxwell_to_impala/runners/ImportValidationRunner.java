package com.maxwell_to_impala.runners;

import com.maxwell_to_impala.holder.MySqlConnectionsHolder;
import com.maxwell_to_impala.model.ColumnDefinition;
import com.maxwell_to_impala.model.MySqlDbMetadata;
import com.maxwell_to_impala.service.ImpalaImportService;
import com.maxwell_to_impala.service.MySqlExportService;
import com.maxwell_to_impala.utils.DatabaseUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Lazy
@Slf4j
@Service
public class ImportValidationRunner extends AbstractRunner {

    @Autowired
    private MySqlExportService mySqlService;

    @Autowired
    private ImpalaImportService impalaImportService;

    @Autowired
    private MySqlConnectionsHolder connectionsHolder;

    @Override
    public void perform() {
        List<MySqlDbMetadata> dbMetadata = connectionsHolder.getDbMetadata();
        Map<String, List<ColumnDefinition>> tablesDefinitions = mySqlService.getTablesDefinition(dbMetadata.get(0));
        Map<Long, List<TableInformation>> mysqlTables = retrieveMySQLTablesInfo(dbMetadata, tablesDefinitions).stream().
                collect(Collectors.groupingBy(TableInformation::getDpId));
        List<TableInformation> impalaList = retrieveImpalaTables(tablesDefinitions);
        List<TableInformation> tablesWithoutDpId = impalaList.stream().filter(tableInformation -> tableInformation != null && tableInformation.getDpId() == null).collect(Collectors.toList());

        log.info(SEPARATOR);
        tablesWithoutDpId.forEach(tableInformation -> log.info("Tables without dpId: {} ", tableInformation.getTableName()));
        log.info(SEPARATOR);
        impalaList = impalaList.stream().
                filter(tableInformation -> tableInformation != null && tableInformation.getDpId() != null).
                collect(Collectors.toList());

        Map<Long, List<TableInformation>> impalaTables =  impalaList.stream().
                collect(Collectors.groupingBy(TableInformation::getDpId));

        Set<Long> missedDbs = new HashMap<>(mysqlTables).keySet();
        missedDbs.removeAll(impalaTables.keySet());
        log.info("There are missed dbs: {}", missedDbs);
        mysqlTables.keySet().removeAll(missedDbs);

        impalaTables.forEach((dpId, impalaTablesInfo) -> {
            List<TableInformation> mysqlTablesInfo = mysqlTables.get(dpId);

            log.info("MySQL DB '{}' ({}/" + mysqlTables.size() + ")", dpId, impalaTablesInfo.size());
            Map<String, List<TableInformation>> impalaTablesByName = impalaTablesInfo.stream().collect(Collectors.groupingBy(TableInformation::getTableName));
            Map<String, List<TableInformation>> mysqlTablesByName = mysqlTablesInfo.stream().collect(Collectors.groupingBy(TableInformation::getTableName));

            // exclude same data
            impalaTablesInfo.removeAll(mysqlTablesInfo);

            impalaTablesInfo.forEach(tableInformation -> {
                TableInformation impalaTable = impalaTablesByName.getOrDefault(tableInformation.getTableName(), new ArrayList<>()).stream().findFirst().orElse(null);
                TableInformation mysql = mysqlTablesByName.getOrDefault(tableInformation.getTableName(), Collections.emptyList()).stream().findFirst().orElse(null);
                if(impalaTable != null && mysql != null) {
                    log.info("\tTable: {}; Count: {}/{}", impalaTable.getTableName(), impalaTable.getCount(), mysql.getCount());
                }

            });
            log.info(SEPARATOR);

        });

    }

    @Data
    private static class TableInformation {
        private static final double ACCEPTABLE_DEVIATION_PERCENTAGE = 0.3;
        private static final double ACCEPTABLE_DEVIATION_EXACT = 100.;

        private Long dpId;
        private String tableName;
        private Long count;
        private String dbType;

        private TableInformation(Long dpId, String tableName, Long count, String dbType) {
            this.dpId = dpId;
            this.tableName = tableName.toLowerCase();
            this.count = count;
            this.dbType = dbType;
        }

        @Override
        public boolean equals(Object o) {
            if(o == null) return false;
            if(!(o instanceof TableInformation)) return false;
            TableInformation other = (TableInformation) o;
            double acceptableDeviation = count * ACCEPTABLE_DEVIATION_PERCENTAGE;
            long deviation = Math.abs(count - other.getCount());
            return dpId.equals(other.getDpId()) && tableName.equals(other.getTableName()) &&
                    (deviation < ACCEPTABLE_DEVIATION_EXACT || deviation <= acceptableDeviation);
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    private List<TableInformation> retrieveImpalaTables(Map<String, List<ColumnDefinition>> tablesDefinitions) {
        ExecutorService executorService = Executors.newFixedThreadPool(16);

        List<TableInformation> impalaTables = new ArrayList<>();

        tablesDefinitions.keySet().forEach(tableName -> executorService.execute(() -> {
            log.info("Start retrieve Impala info for table: {}", tableName);
            try {
                impalaTables.addAll(impalaImportService.rowsCountByDpId(tableName).
                        entrySet().
                        stream().map(entry -> new TableInformation(entry.getKey(), tableName, entry.getValue(), "IMPALA")).
                        collect(Collectors.toList()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            log.info("Finished retrieve info for table: {}", tableName);
        }));

        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to retrieve mysql tables. ", e);
        }
        return impalaTables;
    }

    private List<TableInformation> retrieveMySQLTablesInfo(List<MySqlDbMetadata> dbMetadata, Map<String, List<ColumnDefinition>> tablesDefinitions) {
        List<TableInformation> mysqlTables = new ArrayList<>();

        Map<String, List<MySqlDbMetadata>> connectionsByDbName = dbMetadata.stream().collect(Collectors.groupingBy(MySqlDbMetadata::getDbName));

        String mysqlCountStatement = "select TABLE_SCHEMA, TABLE_NAME, TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE " +
                "TABLE_NAME IN (%s) AND TABLE_SCHEMA IN (%s)";
        Set<String> tablesList = tablesDefinitions.keySet();
        Set<String> databasesList = connectionsByDbName.keySet();

        try (Connection connection = dbMetadata.get(0).getConnection(); Statement statement = connection.createStatement()) {
            String formattedStatement = String.format(mysqlCountStatement,
                    DatabaseUtils.listToInClause(tablesList),
                    DatabaseUtils.listToInClause(databasesList));
            ResultSet resultSet = statement.executeQuery(
                    formattedStatement);
            while (resultSet.next()) {
                String dbName = resultSet.getString(1);
                String tableName = resultSet.getString(2);
                Long rowsCount = resultSet.getLong(3);
                Long dpId = connectionsByDbName.get(dbName).get(0).getDpId();
                if(!rowsCount.equals(0L)) {
                    mysqlTables.add(new TableInformation(dpId, tableName, rowsCount, "MYSQL"));
                }
            }
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mysqlTables;
    }

}
