package com.maxwell_to_impala.service;

import com.maxwell_to_impala.model.ColumnDefinition;
import com.maxwell_to_impala.utils.IOUtils;
import com.maxwell_to_impala.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Slf4j
@Service
public class ImpalaImportService {

    @Autowired
    @Qualifier("impalaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public int getRowsCount(String tableName) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM " + tableName, Integer.class);
    }

    public boolean isRowsCountSame(String firstTable, String secondTable) {
        return getRowsCount(firstTable) == getRowsCount(secondTable);
    }


    /**
     *     @return dpId:count
     */
    public Map<Long, Long> rowsCountByDpId(String tableName) {
        Map<Long, Long> result = new HashMap<>();

        jdbcTemplate.queryForList("SELECT COUNT(*) as rowsCount, dp_id FROM " + tableName + " GROUP BY dp_id").
                forEach(selectData ->
                result.put((Long) selectData.get("dp_id"), (Long) selectData.get("rowsCount")));
        return result;
    }


    public void execute(String query) {
        jdbcTemplate.execute(query);
    }

    public <T> List<T> executeSelect(String query, Function<Map<String, Object>, T> functionMapper) {
         return jdbcTemplate.queryForList(query).stream().map(functionMapper).collect(Collectors.toList());
    }

    public void createExternalTable(String tableName, List<ColumnDefinition> columns, String sourceFile) {
        String query = this.generateCreateExternalTableQuery(tableName, columns, sourceFile);
        this.execute(query);
    }

    public void createParquetTable(List<ColumnDefinition> columns, String tableName) {
        String query = this.generateCreateImpalaParquetTableStatement(columns, tableName);
        this.execute(query);
    }

    public void insertIntoSelect(String source, String destination) {
        String query = "INSERT INTO " + destination + " SELECT * FROM " + source;
        this.execute(query);
    }

    public void dropTableIfExists(String tableName) {
        this.execute("DROP TABLE IF EXISTS " + tableName);
    }

    private String defineImpalaCompatibleDataType(ColumnDefinition column) {
        if (column == null || column.getColumnType() == null) {
            throw new NullPointerException("Column or Type is null");
        }
        switch (column.getColumnDataType().toLowerCase()) {
            case "longtext":
                return "string";
            case "date":
                return "timestamp";
            case "datetime":
                return "timestamp";
            case "smallint":
                return "smallint";
            case "double":
                return "double";
            case "varchar":
                return column.getColumnType();
            case "tinyint":
                if ("tinyint(1)".equals(column.getColumnType())) {
                    return "boolean";
                }
                return "tinyint";
            case "decimal":
                return column.getColumnType();
            case "bigint":
                return "bigint";
            case "int":
                return "int";
            case "timestamp":
                return "timestamp";
            default:
                throw new RuntimeException("Undefined column type: " + column.getColumnType());
        }
    }

    public String generateCreateImpalaParquetTableStatement(List<ColumnDefinition> columns, String tableName) {
        StringBuilder query = new StringBuilder();
        query.append("CREATE TABLE ").append(tableName).append(" (").append("\n");
        Iterator<ColumnDefinition> iterator = columns.iterator();
        while (iterator.hasNext()) {
            ColumnDefinition column = iterator.next();
            query.append("`" + this.changeColumnNameIfNeeded(column.getColumnName()) + "`").append(" ").append(defineImpalaCompatibleDataType(column));
            if (iterator.hasNext()) {
                query.append(",");
            } else {
                query.append(")");
            }
            query.append("\n");
        }
        query.append("STORED AS PARQUET");
        return query.toString();
    }

    private String generateCreateExternalTableQuery(String tableName, List<ColumnDefinition> columns, String sourceFile) {
        StringBuilder query = new StringBuilder();
        query.append("CREATE EXTERNAL TABLE ").append(tableName).append(" (").append("\n");
        Iterator<ColumnDefinition> iterator = columns.iterator();
        while (iterator.hasNext()) {
            ColumnDefinition column = iterator.next();
            query.append(changeColumnNameIfNeeded(column.getColumnName())).append(" ").append(defineImpalaCompatibleDataType(column));
            if (iterator.hasNext()) {
                query.append(",");
            } else {
                query.append(")");
            }
            query.append("\n");
        }
        query.append(" ROW FORMAT DELIMITED FIELDS TERMINATED BY " + StringUtils.wrapWithQuotes(IOUtils.COLUMNS_DELIMITER));
        query.append(" LOCATION '").append(sourceFile).append("';");

        return query.toString();
    }

    private String changeColumnNameIfNeeded(String columnName) {
        switch (columnName.toLowerCase()) {
            case "timestamp":
                return "_timestamp";
            case "date":
                return "_date";
            case "comment":
                return "_comment";
            case "role":
                return "_role";
            case "to":
                return "_to";
            case "from":
                return "_from";
        }
        return columnName;
    }

    public List<ColumnDefinition> getColumns(String tableName) {
        return jdbcTemplate.queryForList("SHOW COLUMN STATS " + tableName).
                stream().
                map(res ->
                new ColumnDefinition(tableName, (String) res.get("Column"), (String) res.get("Type"), null)).
                collect(Collectors.toList());
    }

    public void computeStats(String tableName) {
        jdbcTemplate.execute("COMPUTE STATS " + tableName);
    }

    /**
     * @return new table name
     * */
    public String renameTable(String from, String to) {
        jdbcTemplate.execute("ALTER TABLE " + from + " RENAME TO " + to);
        return to;
    }

    /**
     * @return true if created, false if not
     * */
    public boolean createPartitioningTable(final List<ColumnDefinition> columns, List<String> partitionColumnsNames,
                                           String sourceTable, String destinationTable) {
        List<ColumnDefinition> partitionColumns = partitionColumnsNames.
                stream().
                map(partitionColumnName ->  columns.stream()
                        .filter(column -> column.
                                getColumnName().equalsIgnoreCase(partitionColumnName)).
                                findFirst().
                                orElse(null)).
                filter(Objects::nonNull).
                collect(Collectors.toList());

        if(partitionColumns.size() == 0) {
            return false;
        }

        List<ColumnDefinition> columnsCopyWithoutPartitionedColumns = new ArrayList<>(columns);
        columnsCopyWithoutPartitionedColumns.removeAll(partitionColumns);
        createPartitionedTable(columnsCopyWithoutPartitionedColumns, partitionColumns, destinationTable);
        insertIntoPartitionedTable(columnsCopyWithoutPartitionedColumns, partitionColumns, sourceTable, destinationTable);
        return true;
    }

    private void insertIntoPartitionedTable(final List<ColumnDefinition> columns, List<ColumnDefinition> partitionColumnsNames,
                                            String sourceTable, String destinationTable) {
        String generalColumnsList = generateColumnsListForCreateTable(columns, false);
        String partitionColumnsList = generateColumnsListForCreateTable(partitionColumnsNames, false);

        StringBuilder insertIntoStatement = new StringBuilder();
        insertIntoStatement.append("INSERT INTO ").append(destinationTable);
        insertIntoStatement.append(" ( ").append(generalColumnsList).append(" ) ");
        insertIntoStatement.append(" PARTITION ( ").append(partitionColumnsList).append(" ) ");
        insertIntoStatement.append(" SELECT ").append(generalColumnsList).append(",").append(partitionColumnsList);
        insertIntoStatement.append(" FROM  ").append(sourceTable);
        jdbcTemplate.execute(insertIntoStatement.toString());
    }

    private void createPartitionedTable(List<ColumnDefinition> generalColumns, List<ColumnDefinition> partitionColumns,
                                      String partitionedTableName) {
        StringBuilder createTableStatement = new StringBuilder();
        createTableStatement.append("CREATE TABLE ").append(partitionedTableName).append(" (").append("\n");
        createTableStatement.append(generateColumnsListForCreateTable(generalColumns, true)).append(")");
        createTableStatement.append(" PARTITIONED BY ( ").append(generateColumnsListForCreateTable(partitionColumns, true)).append(")\n");
        createTableStatement.append(" STORED AS PARQUET; ");
        jdbcTemplate.execute(createTableStatement.toString());
    }

    /**
     * @return column_name1 type, column_name2 type ...
     */
    private String generateColumnsListForCreateTable(List<ColumnDefinition> columns, boolean withType) {
        StringBuilder result = new StringBuilder();

        Iterator<ColumnDefinition> generalColumnsIterator = columns.iterator();
        while (generalColumnsIterator.hasNext()) {
            ColumnDefinition column = generalColumnsIterator.next();
            result.append(column.getColumnName());
            if(withType) {
                result.append(" ").append(column.getColumnType());
            }
            if(generalColumnsIterator.hasNext()) {
                result.append(",").append("\n");
            }
        }
        return result.toString();
    }
}
