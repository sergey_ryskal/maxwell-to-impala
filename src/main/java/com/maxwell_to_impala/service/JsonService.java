package com.maxwell_to_impala.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Service
public class JsonService {
    private ObjectMapper objectMapper;
    CollectionType collectionType;

    @PostConstruct
    public void init() {
        this.objectMapper = new ObjectMapper();
        this.collectionType = this.getListCollectionType(Map.class);
    }

    public String objectToString(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Can not convert object to JSON. ", e);
        }
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> toMap(String content) {
        try {
            return this.objectMapper.readValue(content, Map.class);
        } catch (IOException e) {
            throw new RuntimeException("String=>Map conversion failed.", e);
        }
    }

    public <T> List<T> toList(InputStream is, Class<T> tClass) {
        try {
            return this.objectMapper.readValue(is, getListCollectionType(tClass));
        } catch (IOException e) {
            throw new RuntimeException("Failed to convert json. ", e);
        }
    }

    public List<Map<String, Object>> toList(String object) {
        try {
            return this.objectMapper.readValue(object, collectionType);
        } catch (IOException e) {
            throw new RuntimeException("String=>List<Map> conversion failed.", e);
        }
    }

    private <T> CollectionType getListCollectionType(Class<T> tClass) {
        return this.objectMapper.getTypeFactory().constructCollectionType(List.class, tClass);
    }
}
