package com.maxwell_to_impala.service;

import org.apache.commons.io.FileExistsException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.zookeeper.common.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.io.*;


@Lazy
@Service
public class HadoopFsService {

    @Lazy
    @Autowired
    private FileSystem fileSystem;

    public void uploadFile(String source, String dest, boolean override) {
        Path path = new Path(dest);
        try {
            if (fileSystem.exists(path) && override) {
                fileSystem.delete(path, false);
            } else if (fileSystem.exists(path) && !override) {
                throw new FileExistsException("File " + path.toString() + " already exist.");
            }
            try (FSDataOutputStream out = fileSystem.create(path); InputStream in = new BufferedInputStream(new FileInputStream(new File(source)))) {
                IOUtils.copyBytes(in, out, 8196);
                out.flush();
            } catch (IOException e) {
                throw new RuntimeException("Failed to create file", e);
            }
        } catch (IOException io) {
            throw new RuntimeException(io);
        }
    }

    public boolean exists(String path) throws IOException {
        return fileSystem.exists(new Path(path));
    }

    public void deleteFile(String path) throws IOException {
        fileSystem.delete(new Path(path), false);
    }

    /**
     * Remove directory recursively
     */
    public boolean rmr(String path) {
        try {
            if(this.exists(path)) {
                fileSystem.delete(new Path(path), true);
            }
            return true;
        } catch (IOException e) {
            throw new RuntimeException("Failed to drop directory.", e);
        }
    }

    public boolean mkdir(String path) {
        try {
            return fileSystem.mkdirs(new Path(path));
        } catch (IOException e) {
            throw new RuntimeException("Failed to create directory: " + path, e);
        }
    }


}
