package com.maxwell_to_impala.service;


import com.maxwell_to_impala.config.ApplicationConfig;
import com.maxwell_to_impala.model.ColumnDefinition;
import com.maxwell_to_impala.model.MySqlDbMetadata;
import com.maxwell_to_impala.utils.IOUtils;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MySqlExportService {

    @Autowired
    private ApplicationConfig applicationConfig;

    /**
     * @return result key: 'file_path' value: 'db_name'
     */
    public Pair<String, String> tableToTsv(String tableName, MySqlDbMetadata mySqlDbMetadata, List<ColumnDefinition> columns) {
        String resultFilePath = IOUtils.buildTmpFilePath(mySqlDbMetadata.getDbName(), tableName);
        String exportStatement = generateMySqlExportStatement(tableName, columns, mySqlDbMetadata);

        try (Connection connection = mySqlDbMetadata.getConnection();
             PreparedStatement preparedStatement =  connection.prepareStatement(exportStatement, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)){
            preparedStatement.setFetchSize(Integer.MIN_VALUE);
            ResultSet resultSet = preparedStatement.executeQuery();
            String csvPath = IOUtils.convertToTsv(resultSet, resultFilePath,
                    (r) -> r + IOUtils.COLUMNS_DELIMITER + mySqlDbMetadata.getDpId().toString());
            resultSet.close();
            if(csvPath == null) {
                return null;
            }

            return new Pair<>(csvPath, mySqlDbMetadata.getDbName());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String generateMySqlExportStatement(String tableName, List<ColumnDefinition> columns, MySqlDbMetadata dbMetadata) {
        StringBuilder result = new StringBuilder();
        String booleanWrapper = "if(%s, 'true', 'false')";
        result.append("SELECT ");

        Map<String, List<ColumnDefinition>> columnsDefsByName = columns.stream().collect(Collectors.groupingBy(ColumnDefinition::getColumnName));

        Iterator<String> columnsNameIter = columns.stream().map(ColumnDefinition::getColumnName).
                collect(Collectors.toList()).iterator();

        while (columnsNameIter.hasNext()) {
            String currentColumn = columnsNameIter.next();
            List<ColumnDefinition> columnDefinitions = columnsDefsByName.get(currentColumn);
            if ("tinyint(1)".equals(columnDefinitions.get(0).getColumnType())) {
                result.append(String.format(booleanWrapper, currentColumn));
            } else {
                result.append(currentColumn);
            }

            if (columnsNameIter.hasNext()) {
                result.append(",");
            }
        }
        result.append(" FROM ").append(dbMetadata.getDbName()).append(".").append(tableName);
        return result.toString();
    }

    public Map<String, List<ColumnDefinition>> getTablesDefinition(MySqlDbMetadata mySqlDbMetadata) {
        List<ColumnDefinition> columns = new ArrayList<>();

        String query = "SELECT table_name, column_name, data_type, column_type" +
                " FROM information_schema.columns" +
                " WHERE table_schema = ? " +
                " and column_name NOT LIKE '_Import%' " +
                " and column_name NOT LIKE 'Import%' " +
                " and column_name <> 'FreshInserted'";

        try (Connection connection = mySqlDbMetadata.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, mySqlDbMetadata.getDbName());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String tableName = resultSet.getString("table_name");

                if(!applicationConfig.getExcludeTables().contains(tableName.toLowerCase())) {
                    ColumnDefinition column = new ColumnDefinition();
                    column.setTableName(tableName);
                    column.setColumnName(resultSet.getString("column_name"));
                    column.setColumnDataType(resultSet.getString("data_type"));
                    column.setColumnType(resultSet.getString("column_type"));
                    columns.add(column);
                }

            }
            resultSet.close();
            return columns.stream().collect(Collectors.groupingBy(ColumnDefinition::getTableName));
        } catch (SQLException e) {
            throw new RuntimeException("Can not retrieve database schema. Database: " + mySqlDbMetadata.getUrl(), e);
        }
    }
}
