package com.maxwell_to_impala.service;

import com.maxwell_to_impala.config.RedashConfiguration;
import com.maxwell_to_impala.model.RedashQuery;
import com.maxwell_to_impala.utils.IOUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

@Lazy
@Slf4j
@Service
public class RedashAPIService {

    public static final Charset DEFAULT_ENCODING = Charset.forName("UTF-8");
    @Autowired
    private RedashConfiguration redashConfiguration;

    @Autowired
    private JsonService jsonService;
    @Getter
    private Integer datasourceId;

    private HttpClient httpClient;
    private Map<String, List<RedashQuery>> redashQueries;

    @PostConstruct
    public void init() {
        this.httpClient = new DefaultHttpClient();
        this.datasourceId = this.retrieveDatasourceId(redashConfiguration.getDatasource());
        this.redashQueries = this.getRedashQueries();
    }

    public List<RedashQuery> getQueriesByName(RedashQuery query) {
        Map<String, List<RedashQuery>> redashQueries = getRedashQueries();
        return redashQueries.get(query.getName());
    }

    public void upsertQuery(RedashQuery queryForSave) {
        List<RedashQuery> redashQueries = this.redashQueries.getOrDefault(queryForSave.getName(), Collections.EMPTY_LIST);
        if(redashQueries.size() == 0) {
            this.createOrUpdateQuery(queryForSave, null);
            log.info("Create {} query. ", queryForSave.getName());
        } else if(redashQueries.size() == 1) {
            this.createOrUpdateQuery(queryForSave, redashQueries.get(0).getId());
            log.info("Update {} query. ", queryForSave.getName());
        } else if(redashQueries.size() > 1) {
            log.warn("Can not update redash query. There are several queries with name: {}, for datasource: {}",
                    queryForSave.getName(), queryForSave.getDatasourceId());
        }
    }

    private void createOrUpdateQuery(RedashQuery queryForSave, Integer queryId) {
        String query = queryId == null ? this.queryRequest() : this.queryUpdateRequest(queryId);
        HttpPost httpPost = new HttpPost(query);

        String queryObject = jsonService.objectToString(queryForSave);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        HttpResponse response = null;
        try {
            httpPost.setEntity(new StringEntity(queryObject));
            response = this.httpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode != 200 && statusCode != 201) {
                throw new IllegalStateException("Can not create query. Status code: " +
                        statusCode + ". Reason: " + response.getStatusLine().getStatusCode());
            }
            IOUtils.saveCloseHttpResponse(response);
        } catch (IOException e) {
            throw new RuntimeException("Can not create redash query. ", e);
        }
    }

    private Map<String, List<RedashQuery>> getRedashQueries() {
        List<RedashQuery> result = new ArrayList<>();
        try {
            int currentPage = 1;
            while (true) {
                HttpGet httpGet = new HttpGet(this.queryRequest("page", String.valueOf(currentPage++)));
                HttpResponse httpResponse = this.httpClient.execute(httpGet);
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                try {
                    if(statusCode == 200) {
                        String queriesJson = org.apache.commons.io.IOUtils.toString(httpResponse.getEntity().getContent(), DEFAULT_ENCODING);
                        Map<String, Object> queryResult = jsonService.toMap(queriesJson);
                        result.addAll(((List<Map<String, Object>>) queryResult.get("results")).stream()
                                .map(query -> {
                                    RedashQuery redashQuery = new RedashQuery();
                                    redashQuery.setDatasourceId(this.datasourceId);
                                    redashQuery.setName((String) query.get("name"));
                                    redashQuery.setQuery((String) query.get("query"));
                                    redashQuery.setId((Integer) query.get("id"));
                                    return redashQuery;
                                })
                                .collect(Collectors.toList()));
                    } else {
                        break;
                    }
                } finally {
                    IOUtils.saveCloseHttpResponse(httpResponse);
                }

            }
            return result.stream().collect(Collectors.groupingBy(RedashQuery::getName));
        } catch (IOException e) {
            throw new RuntimeException("Can not retrieve redash queryies. ", e);
        }
    }




    private Integer retrieveDatasourceId(String datasourceName) {
        String url = this.datasourceRequest();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse execute = this.httpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if(statusCode == 200) {
                String requestResult = org.apache.commons.io.IOUtils.toString(execute.getEntity().getContent(), DEFAULT_ENCODING);

                Optional<Map<String, Object>> datasource = jsonService.toList(requestResult)
                        .stream()
                        .filter(el -> el.get("name") != null && datasourceName.equals(el.get("name")))
                        .findFirst();
                if(datasource.isPresent()) {
                    return (Integer) datasource.get().get("id");
                } else {
                    throw new IllegalStateException("There is no datasource: " + datasourceName + ". " + url);
                }
            } else {
                throw new IllegalStateException("Can not retrieve datasource id. " + datasourceName + url);
            }
        } catch (IOException e) {
            throw new RuntimeException("Can not retrieve datasource id");
        }
    }

    private String datasourceRequest() {
        return getRequest("api/data_sources");
    }

    /**
     * args show be odd
     * */
    private String queryRequest(String ... args) {
        return getRequest("api/queries", args);
    }

    /**
     * args should be odd
     * */
    private String queryUpdateRequest(Integer queryId, String ... args) {
        return getRequest("api/queries/" + queryId, args);
    }

    private String getRequest(String apiPart, String ... args) {
        if(args.length == 0 || (args.length > 0 && args.length % 2 == 0)) {
            StringBuilder result = new StringBuilder();
            result.append(redashConfiguration.getHost());
            result.append(apiPart);
            result.append("?api_key=" + redashConfiguration.getApiKey());
            for (int i = 0; i < args.length / 2; i++) {
                result.append("&")
                        .append(args[i])
                        .append("=")
                        .append(args[i + 1]);
            }
            return result.toString();
        } else {
            throw new IllegalStateException("Number of args should be odd");
        }
    }


}
