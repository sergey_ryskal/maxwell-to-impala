package com.maxwell_to_impala.holder;

import com.maxwell_to_impala.config.ApplicationConfig;
import com.maxwell_to_impala.model.FactsTable;
import com.maxwell_to_impala.service.JsonService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.List;


@Lazy
@Slf4j
@Service
public class FactsTablesDefinitionsHolder {
    @Lazy
    @Autowired
    public ApplicationConfig applicationConfig;

    @Autowired
    JsonService jsonService;

    @Getter
    private List<FactsTable> factsTables;

    @PostConstruct
    public void init() {
        InputStream definitionFile = this.getClass().getClassLoader().getResourceAsStream(applicationConfig.getFactsTableDefinitionFile());
        factsTables = jsonService.toList(definitionFile, FactsTable.class);
    }

}
