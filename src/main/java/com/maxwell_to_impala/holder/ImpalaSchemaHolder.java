package com.maxwell_to_impala.holder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Lazy
@Slf4j
@Service
public class ImpalaSchemaHolder {

    @Lazy
    @Autowired
    @Qualifier("impalaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Getter
    private List<ViewEntity> views;

    @Getter
    private List<String> tables = new ArrayList<>();

    @PostConstruct
    public void init() {
        /*this process is very slow locally*/
        views = jdbcTemplate.queryForList("SHOW TABLES", String.class)
                .parallelStream()
                .map(table -> {
                    String statement = "SHOW CREATE TABLE " + table;
                    String createQuery = jdbcTemplate.queryForObject(statement, String.class);
                    if(createQuery.startsWith("CREATE VIEW")) {
                        return new ViewEntity(table, createQuery);
                    } else {
                        tables.add(table);
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        log.info("Schema has been initialized.");
    }

    @Data
    @AllArgsConstructor
    public static class ViewEntity {
        protected String name;
        protected String createStatement;
    }
}
