package com.maxwell_to_impala.holder;

import com.maxwell_to_impala.config.ApplicationConfig;
import com.maxwell_to_impala.config.MySqlConnectionConfiguration;
import com.maxwell_to_impala.model.MySqlDbMetadata;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Lazy
@Service
public class MySqlConnectionsHolder {

    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private MySqlConnectionConfiguration mySqlConfiguration;

    @Getter
    private List<MySqlDbMetadata> dbMetadata = new ArrayList<>();

    @PostConstruct
    public void init() throws SQLException {
        String url = mySqlConfiguration.getUrl();
        Map<String, Long> deploymentMetadata = getDeploymentMetadata();

        try (Connection generalConnection = DriverManager.getConnection(url);
             ResultSet catalogs = generalConnection.getMetaData().getCatalogs()) {
            while (catalogs.next()) {
                String dbName = catalogs.getString("TABLE_CAT");
                if(applicationConfig.isDatabaseForImport(dbName)) {
                    String dbNameDpPart = dbName.split("_")[1];
                     // This hardcode is related to mistake in Aurora, probably some day it will be fixed @Ivan
                     // That one case is `scklimek.salonultimate.com`, the actual database is `zenith_sckilmek`
                    Long dpId = deploymentMetadata.get(dbNameDpPart.equals("sckilmek") ? "scklimek" : dbNameDpPart);
                    if(dpId == null) {
                        throw new RuntimeException("Can not define dpId: " + dbName);
                    }
                    dbMetadata.add(new MySqlDbMetadata(dbName, mySqlConfiguration.getUrl(dbName), dpId));
                }
            }
        }
//        create tmp folders
        dbMetadata.forEach(db -> {
            File file = new File("/tmp/" + db.getDbName());
            if(file.exists()) {
                file.delete();
            }
            file.mkdir();
        });
    }

//  <DeploymentName, DeploymentId>
    private Map<String, Long> getDeploymentMetadata() {
        Map<String, Long> result = new HashMap<>();

        try (Connection metadataDbConnection = DriverManager.getConnection(mySqlConfiguration.getUrl(applicationConfig.getMysqlMetadataDatabase()));
             Statement statement = metadataDbConnection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT Url, Id FROM Deployments")) {
            while (resultSet.next()) {
                String url = resultSet.getString("Url");
                result.put(url.split("\\.")[0], resultSet.getLong("Id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Can not connect to metadata database", e);
        }
        return result;
    }
}
