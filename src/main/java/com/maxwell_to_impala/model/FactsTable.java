package com.maxwell_to_impala.model;


import com.maxwell_to_impala.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.time.DateFormatUtils;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FactsTable {
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    private String tableFilePath;
    private String tableName;
    private String viewFilePath;
    private String viewName;

    private List<String> partitionFields;
    private String rangeConditionColumn;
    private String rangeConditionColumnType;
    private String validationByCountTableName;

    public String conditionColumnValueToString(Object obj) {
        if(obj == null) {
            return null;
        }
        switch (this.rangeConditionColumnType) {
            case "DATETIME":
                return StringUtils.wrapWithQuotes(DateFormatUtils.format(((Timestamp) obj).getTime(), DEFAULT_DATE_FORMAT));
            default:
                return this.rangeConditionColumn;
        }
    }

}

