package com.maxwell_to_impala.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ColumnDefinition {
    private String tableName;
    private String columnName;
    private String columnType;
    private String columnDataType;
}