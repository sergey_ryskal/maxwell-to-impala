package com.maxwell_to_impala.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Data
@NoArgsConstructor
public class MySqlDbMetadata {
    private final static Integer POOL_SIZE = 32;
    public static final String MYSQL_DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
    private String dbName;
    private String url;
    private Long dpId;

    private DataSource dataSource;

    public MySqlDbMetadata(String dbName, String url, Long dpId) {
        this.dbName = dbName;
        this.url = url;
        this.dpId = dpId;

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(MYSQL_DRIVER_CLASS);
        dataSource.setUrl(this.getUrl());
        dataSource.setInitialSize(POOL_SIZE);
        this.dataSource = dataSource;
    }

    public Connection getConnection() throws SQLException {
        Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }
}
