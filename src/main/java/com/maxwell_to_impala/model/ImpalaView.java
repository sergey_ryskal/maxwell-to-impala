package com.maxwell_to_impala.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ImpalaView {
    protected String name;
    protected String createStatement;
}
