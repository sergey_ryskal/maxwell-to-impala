package com.maxwell_to_impala.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RedashQuery {
    private String name;
    private String query;
    @JsonProperty(value = "data_source_id")
    private Integer datasourceId;
    @JsonIgnore
    private Integer id;

    public RedashQuery(String name, String query, Integer datasourceId) {
        this(name, query, datasourceId, null);
    }
}
