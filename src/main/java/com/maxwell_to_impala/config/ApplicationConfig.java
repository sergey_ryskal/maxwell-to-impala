package com.maxwell_to_impala.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@PropertySource("classpath:application.yml")
public class ApplicationConfig {

    @Getter
    @Value("${com.zenith.app.tmp_dir}")
    private String tmpDir;

    @Getter
    @Value("${com.zenith.app.metadata_mysql_database}")
    private String mysqlMetadataDatabase;

    @Getter
    @Value("#{'${com.zenith.app.include_db_patterns}'.split(',')}")
    private List<String> databaseForImportPatterns;

    @Getter
    @Value("#{'${com.zenith.app.exclude_db_patterns}'.split(',')}")
    private List<String> excludeDatabaseForImportPatterns;

    @Getter
    @Value("#{'${com.zenith.app.exclude_tables}'.split(',')}")
    private List<String> excludeTables;

    @Getter
    @Value("#{'${com.zenith.app.partition_columns}'.split(',')}")
    private List<String> partitionColumns;

    @Getter
    @Value("${com.zenith.app.facts_table_dir}")
    private String factsTableDefinitionFile;

    @Getter
    @Value("${com.zenith.app.partition_threshold}")
    private Integer partitionThreshold;


    @PostConstruct
    public void init() {
        File file = new File(tmpDir);
        if (!file.exists() && !file.mkdir()) {
            throw new RuntimeException("Can not create tmp dir: " + tmpDir);
        }
        excludeTables = excludeTables.stream().map(String::toLowerCase).collect(Collectors.toList());
        partitionColumns = partitionColumns.stream().map(String::toLowerCase).collect(Collectors.toList());
    }

    public boolean isDatabaseForImport(String dbName) {
        if(databaseForImportPatterns == null || databaseForImportPatterns.size() == 0) {
            return true;
        }
        boolean shouldBeInclude = false;

        for(String dbPatternName : databaseForImportPatterns) {
            if(dbName.startsWith(dbPatternName)) {
                shouldBeInclude = true;
            }
            break;
        }
        if(shouldBeInclude) {
            for (String dbPatterName : excludeDatabaseForImportPatterns) {
                if(dbName.startsWith(dbPatterName)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}

