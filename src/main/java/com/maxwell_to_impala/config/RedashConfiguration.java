package com.maxwell_to_impala.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedashConfiguration {

    @Getter
    @Value("${redash.host}")
    private String host;
    @Getter
    @Value("${redash.api_key}")
    private String apiKey;
    @Getter
    @Value("${redash.datasource_name}")
    private String datasource;

}
