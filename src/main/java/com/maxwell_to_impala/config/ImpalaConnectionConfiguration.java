package com.maxwell_to_impala.config;

import lombok.Getter;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;


@Configuration
public class ImpalaConnectionConfiguration {

    private static final String IMPALA_DRIVER = "com.cloudera.impala.jdbc41.Driver";
    private static final String JDBC_URL_TEMPLATE = "jdbc:impala://%s:%s/%s";

    @Value("${impala.host:localhost}")
    private String host;
    @Value("${impala.port:21050}")
    private String port;
    @Value("${impala.user:#{null}}")
    private String user;
    @Value("${impala.password:#{null}}")
    private String password;
    @Value("${impala.pool}")
    private String poolSize;
    @Getter
    @Value("${impala.db:test}")
    private String dbName;

    @PostConstruct
    public void init() {
        try {
            Class.forName(IMPALA_DRIVER);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Check Impala Driver. ", e);
        }
    }

    /**
     * destroyMethod = "", because of spring feature, it's try to close BasicDataSource twice
     * */
    @Lazy
    @Primary
    @Bean(name = "impalaDatasource", destroyMethod = "")
    public DataSource createDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(IMPALA_DRIVER);
        dataSource.setUrl(this.getUrl());
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        dataSource.setInitialSize(Integer.parseInt(poolSize));
        return dataSource;
    }

    @Lazy
    @Primary
    @Bean(name = "impalaJdbcTemplate")
    public JdbcTemplate createImpalaJdbcTemplate(@Autowired @Qualifier("impalaDatasource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    private String getUrl() {
        return String.format(JDBC_URL_TEMPLATE, host, port, dbName);
    }

}
