package com.maxwell_to_impala.config;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

@Configuration
public class MySqlConnectionConfiguration {

    @Value("${mysql.host:localhost}")
    private String host;
    @Value("${mysql.port:3306}")
    private String port;
    @Value("${mysql.user:#{null}}")
    private String user;
    @Value("${mysql.password:#{null}}")
    private String password;
    @Value("#{'${mysql.options}'.split(',')}")
    private List<String> options;

    public String getUrl() {
        return getUrl(null);
    }

    public String getUrl(String dbName) {
        StringBuilder url = new StringBuilder();
        url.append("jdbc:mysql://");
        url.append(this.host).append(":").append(this.port).append("/");
        if(dbName != null) {
            url.append(dbName);
        }

        if(this.user != null) {
            url.append("?user=").append(this.user);
            if(this.password != null) {
                url.append("&").append("password=").append(this.password);
            }
        }

        if(this.user != null && options.size() > 0) {
            url.append("&");
        } else if(this.user == null && options.size() > 0) {
            url.append("?");
        }
        url.append(String.join("&", options));

        return url.toString();
    }

}
