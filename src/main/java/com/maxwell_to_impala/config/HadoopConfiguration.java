package com.maxwell_to_impala.config;

import lombok.Getter;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;

@Configuration
public class HadoopConfiguration {
    private static final String FS_ROOT_URI_TEMPLATE = "hdfs://%s:%s";

    @Value("${hadoop.host}")
    private String host;
    @Value("${hadoop.port}")
    private String port;

    @Getter
    @Value("${hadoop.base_dir:/zenith}")
    private String baseDir;

    @PostConstruct
    public void init() {
        System.setProperty("hadoop.home.dir", "/");
        try (FileSystem fs = FileSystem.get(getUri(FS_ROOT_URI_TEMPLATE), getConfig())) {
            Path basePath = new Path(this.baseDir);
            if (!fs.exists(basePath)) {
                fs.mkdirs(basePath);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private org.apache.hadoop.conf.Configuration getConfig() {
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        return conf;
    }

    @Bean(destroyMethod = "close")
    @Primary
    public FileSystem createFileSystemConnection() {
        try {
            return FileSystem.get(getUri(FS_ROOT_URI_TEMPLATE + baseDir), getConfig());
        } catch (IOException e) {
            throw new RuntimeException("Failed to create hadoop fs.", e);
        }
    }

    private URI getUri(String template) {
        return URI.create(String.format(template, host, port));
    }

}
