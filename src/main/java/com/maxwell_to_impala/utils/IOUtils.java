package com.maxwell_to_impala.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.function.Function;

@Slf4j
@Service
public class IOUtils {
    private static final String TMP_FILE_TEMPLATE = "/tmp/%s/%s";
    public static final String NULL_VALUE = "\\N";
    public static final String COLUMNS_DELIMITER = "\t";

    private ClassLoader classLoader;

    @PostConstruct
    public void init() {
        this.classLoader = this.getClass().getClassLoader();
    }

    /***
     *
     * return null if result set is empty
     * */
    public static String convertToTsv(ResultSet rs, String file, Function<StringBuilder, String> lineProcessor) {
        try {
            if(!rs.next()) {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try (BufferedWriter csvWriter = new BufferedWriter(new PrintWriter((new File(file))))) {
            ResultSetMetaData meta = rs.getMetaData();
            int numberOfColumns = meta.getColumnCount();
            do {
                StringBuilder csvLine = new StringBuilder();
                for (int i = 1; i <= numberOfColumns; i++) {
                    Object object = rs.getObject(i);
                    if(object == null) {
                        csvLine.append(NULL_VALUE);
                    } else {
                        csvLine.append(object.toString().replaceAll("\\t", " "));
                    }
                    if (i < numberOfColumns) {
                        csvLine.append(COLUMNS_DELIMITER);
                    }
                }
                csvWriter.write(lineProcessor.apply(csvLine));
                csvWriter.write("\n");
            }
            while (rs.next());
            csvWriter.flush();
        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    public static String buildTmpFilePath(String dbName, String fileName) {
        return String.format(TMP_FILE_TEMPLATE, dbName, fileName);
    }

    public static void saveCloseHttpResponse(HttpResponse response) {
        if(response != null && response.getEntity() != null) {
            try (InputStream inputStream = response.getEntity().getContent()) {
                /*do nothing expected*/
            } catch (IOException e) {
                log.warn("Can not close HttpResponse.", e);
            }
        }
    }
    public String getFileContent(String path) {
        try {
            return org.apache.commons.io.IOUtils.toString(classLoader.getResourceAsStream(path), "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException("Failed to retrieve file content: " + path, e);
        }
    }
}
