package com.maxwell_to_impala.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RetriesUtils {

    @FunctionalInterface
    public interface LambdaFunction {
        void invoke();
    }


    public static void executeWithRetries(LambdaFunction lambdaFunction) {
        executeWithRetries(lambdaFunction, 10, 5000);
    }
    public static void executeWithRetries(LambdaFunction lambdaFunction, int retriesCount, int delay) {
        while (retriesCount != 0) {
            try {
                lambdaFunction.invoke();
                return;
            } catch (Exception e) {
                log.info("", e);
            } finally {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                retriesCount --;
            }
        }
        log.info("Retries count expired. ");
    }
}
