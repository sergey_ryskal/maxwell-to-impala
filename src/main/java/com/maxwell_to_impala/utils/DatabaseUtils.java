package com.maxwell_to_impala.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.stream.Stream;

@Slf4j
public class DatabaseUtils {

    public static String listToInClause(Collection<String> list) {
        return listToString(list, true);
    }

    public static String listToString(Collection<String> list) {
        return listToString(list, false);
    }

    private static String listToString(Collection<String> list, boolean wrapWithQuotes) {
        Stream<String> stream = list.stream();
        if(wrapWithQuotes) {
            stream = stream.map(StringUtils::wrapWithQuotes);
        }
        return stream.reduce((s, s2) -> s + "," + s2).get();
    }

}
