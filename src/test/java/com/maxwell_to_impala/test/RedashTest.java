package com.maxwell_to_impala.test;


import com.maxwell_to_impala.model.RedashQuery;
import com.maxwell_to_impala.service.RedashAPIService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class RedashTest extends AbstractTest {

    @Autowired
    private RedashAPIService redashService;

    @Test
    public void upsertTest() {
        RedashQuery redashQuery = new RedashQuery("JavaQueryTest", "select 1", redashService.getDatasourceId());

        redashService.upsertQuery(redashQuery);
        List<RedashQuery> queries = redashService.getQueriesByName(redashQuery);
        Assert.assertEquals(queries.size(), 1);
        Integer oldId = queries.get(0).getId();
        redashService.init();
        redashQuery.setQuery("select 2");
        redashService.upsertQuery(redashQuery);
        List<RedashQuery> updatedQueries = redashService.getQueriesByName(redashQuery);
        Assert.assertEquals(1, updatedQueries.size());
        Integer newId = updatedQueries.get(0).getId();
        Assert.assertEquals(oldId, newId);

    }
}
